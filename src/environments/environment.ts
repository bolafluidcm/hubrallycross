// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  apiEndpoint: 'http://api.rallycrossrx.com/api/',
  currentEvent: 103,
  currentYear: 2017,
  heatsUrl: 'allheats',
  entrantsUrl: 'entrants',
  supercar: 1,
  super1600: 2,
  touringcar: 3,
  rx2:  4,
  erx: 5,
  sundayEvevningNewsUrl: 'racedaynews',
  sundayEvevningNewsCat: 'youtube',
  sundayEvevningNewsKeyword: 'france',
  flagsUrl: 'http://www.fiaworldrallycross.com/_img/_flags/',
  manufacturersUrl: 'http://www.fiaworldrallycross.com/_img/_manufacturers/',
  carsUrls: 'http://www.fiaworldrallycross.com/_mediaimages/imagesource.php?image=',
  carSmallSize: '&maxwidth=158',
  carLargeSize: '&maxwidth=225',
  missingCarSmall: 'http://www.fiaworldrallycross.com/_mediaimages/imagesource.php?image=car_missing.png&maxwidth=158',
  missingCarLarge: 'http://www.fiaworldrallycross.com/_mediaimages/imagesource.php?image=car_missing.png&maxwidth=225',
  missingBadge: 'assets/images/manufacturers/badge_missing.png',
  rx2Home: 'http://www.fiaworldrallycross.com/rx2',

};
