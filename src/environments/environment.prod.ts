export const environment = {
  production: true,
  apiEndpoint: 'http://api.rallycrossrx.com/api/',
  currentEvent: 100,
  currentYear: 2017,
  heatsUrl: 'heats',
  supercar: 1,
  super1600: 2,
  touringcar: 3,
  rx2:  4,
  erx: 5
};
