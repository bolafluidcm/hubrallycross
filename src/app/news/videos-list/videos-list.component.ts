import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Routes, RouterModule, Router } from '@angular/router';


@Component({
  selector: 'app-videos-list',
  templateUrl: './videos-list.component.html',
  providers: [ NewsService ],
  styles: []
})
export class VideosListComponent implements OnInit {
  errorMessage: string;
  news: any[];
  mode = 'Observable';
  //router;



  config: Object = {
    pagination: '.swiper-pagination',
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    slidesPerView: 4,
    loop:true,
    breakpoints: {
      1024: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  };

  constructor(private newsService: NewsService, private _router: Router ) { }

  ngOnInit() {
    this.getCurrentNews();
    }


  getCurrentNews() {
    this.newsService.getCurrentNews()
      .subscribe(
        news => this.news = news,
        error =>  this.errorMessage = <any>error);
  }

  showNewsItemDetail(newsItem){
    this._router.navigate(['/videos', newsItem.id]);
  }


}
