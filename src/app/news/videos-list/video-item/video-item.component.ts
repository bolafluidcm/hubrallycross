import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsService } from '../../news.service';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SafehtmlPipe } from '../../../shared/pipes/safehtml-pipe';

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  providers: [ NewsService ]
})
export class VideoItemComponent implements OnInit {
	sub: any;

	public videoItem;

  constructor(private newsService: NewsService, private activatedRoute: ActivatedRoute, private router: Router) {
		}

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];
    console.log(id);
    this.newsService.getNewsItemDetail(id)
	      .subscribe(
        data => {
          const blogArray = [];
          for (let key in data) {
            blogArray.push(data[key]);
          }
          this.videoItem = blogArray;
        }

      )

		}

}

