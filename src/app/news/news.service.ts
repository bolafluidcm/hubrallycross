import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { News } from './news';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment'


@Injectable()
export class NewsService {
  public news: News;
  private apiUrl = environment.apiEndpoint;
  private newsUrl = this.apiUrl + 'news';
  sundayEvevningNewsUrl = environment.sundayEvevningNewsUrl;
  sundayEvevningNewsCat = environment.sundayEvevningNewsCat;
  sundayEvevningNewsKeyword= environment.sundayEvevningNewsKeyword;


  constructor (private http: Http) {}

  getSundayEveningNews(): Observable<News[]> {
    return this.http.get(this.apiUrl + this.sundayEvevningNewsUrl + '/' + this.sundayEvevningNewsCat + '/' + this.sundayEvevningNewsKeyword)
        .map(response => response.json() as News[])
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getNewsItemDetail(id): Observable<any>{
    const url = this.newsUrl + "/" + id;
    return this.http.get(url)
      .map(res => {
        const newsItem = res.json();
        return newsItem;
      })
  }

  getNews(): Observable<any[]> {
    return this.http.get(this.newsUrl)
      .map(response => response.json() as News[])
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }


  getCurrentNews(): Observable<News[]> {
    return this.http.get(this.newsUrl)
      .map(response => response.json() as News[])
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }



  getNewsItem(id) {
    return this.http.get(this.newsUrl + "/" + id)
      .map((response: Response) => response.json());
  }


  getSelectedNews(id): Observable<News> {
    return this.http.get(this.newsUrl + "/" + id)
      .do( res => console.log('HTTP response:', res) )
      .map(response => response.json() as News)
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  get(id): Observable<News> {
    let news$ = this.http
      .get(this.newsUrl + "/" + id, {headers: this.getHeaders()})
      .map(mapNews);
      return news$;
  }


    private getHeaders(){
     let headers = new Headers();
     headers.append('Accept', 'application/json');
     return headers;
   }
  //
  // create(name: string): Observable<Driver> {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //
  //   return this.http.post(this.driversUrl, { name }, options)
  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }

  //// private extractData(res: Response) {
  ////   let body = res.json() as Driver[];
  ////   return body.data || { };
  //// }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

   function mapNews(response:Response): News{
     return toNews(response.json());
   }

   function toNews(r:any): News{
     let news = <News>({
       id: r.id,
       title: r.title,
       content: r.content,
       image: r.image,
       url: r.url,
     });
       console.log('Parsed news:', news);

     return news;
   }

  //function extractId(newsData:any){
  //  let extractedId = newsData.url.replace('http://api.rallycrossrx.com/api/news/','').replace('/','');
  //  return parseInt(extractedId);
  //}


