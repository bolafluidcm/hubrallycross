import { Component, OnInit, OnDestroy, ViewChild, Input, Renderer, ElementRef } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { JsonParseService } from '../services/json-parse.service'
// import { SwiperComponent } from 'angular2-useful-swiper';
import { Schedule } from '../schedule/schedule';
import { ScheduleService } from '../schedule/schedule.service';
import _ from 'lodash';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  providers: [ ScheduleService ]

})
export class EventsComponent implements OnInit {
  index:number;
  schedule: Schedule;
  // schedules: Schedule[];
  schedules: any;
  kolos$;

  // @ViewChild('usefulSwiper') usefulSwiper: SwiperComponent;
  // config: SwiperOptions = {
  //   pagination: '.swiper-pagination',
  //   paginationClickable: true,
  //   nextButton: '.swiper-button-next',
  //   prevButton: '.swiper-button-prev',
  //   slidesPerView: 7,
  //   spaceBetween: 30,
  //   initialSlide:11
  // };
  constructor( private scheduleService: ScheduleService) {
    // this.mainSlide();
    // scheduleService.getRxEVents().subscribe(p=>this.schedules = p);
    // this.kolos$ = scheduleService.getRxEVents();

    // this.scheduleService.getSchedules().subscribe((schedules) => {
    //   const currentTime = new Date().toLocaleTimeString('en-GB');
    //   this.schedules = schedules;
    //   this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
    //   if(this.index > 0){
    //     this.index = this.index-1;
    //   }else {
    //     this.index = 0;
    //   }
    //     }
    // )

  }

  ngOnInit(){
  this.koko();
  }
  ngDoCheck(){
  }
  ngAfterContentInit(){
    // this.koko();
  }

  next() {
    // this.usefulSwiper.swiper.slideTo(7);
  }
  ngAfterViewInit(){
    // this.next();
    // this.usefulSwiper.swiper.slideTo(9);


  }

  koko() {
    return this.scheduleService.getSchedules().subscribe((schedules) => {
        const currentTime = new Date().toLocaleTimeString('en-GB');
        this.schedules = schedules;
        this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
        if(this.index > 0){
          this.index = this.index-1;
        }else {
          this.index = 0;
        }

        //
        // this.idaho = this.index;
        // this.scheduleService.addCount(this.idaho);
      // this.usefulSwiper.swiper.slideTo(this.index);
        // this.usefulSwiper.params.slideTo(this.index);

      }
    )

    // console.log(this.index);
  }

  mainSlide(): void {
    this.scheduleService.getSchedules().subscribe((schedules) => {
        const currentTime = new Date().toLocaleTimeString('en-GB');
        this.schedules = schedules;
        this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
        if(this.index > 0){
          this.index = this.index-1;
        }else {
          this.index = 0;
        }

        //
        // this.idaho = this.index;
        // this.scheduleService.addCount(this.idaho);
        // this.usefulSwiper.swiper.slideTo(this.index);
        // this.usefulSwiper.params.slideTo(this.index);

      }
    )

    // console.log(this.index);
  }


  }
