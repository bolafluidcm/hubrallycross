// import { Component, OnInit, OnDestroy, ViewChild, Input, Renderer, ElementRef } from '@angular/core';
// import { Schedule } from '../schedule/schedule';
// import { ScheduleService } from '../schedule/schedule.service';
// import _ from 'lodash';
// import { SwiperComponent, SwiperDirective, SwiperConfigInterface } from 'ngx-swiper-wrapper';
//
//
// @Component({
//   moduleId: module.id + '',
//   selector: 'app-eventrx',
//   templateUrl: './eventrx.component.html',
//   styleUrls: ['./eventrx.component.css'],
//   providers: [ ScheduleService ]
// })
// export class EventrxComponent implements OnInit {
//   @ViewChild(SwiperComponent) componentRef: SwiperComponent;
//   @ViewChild('directiveRef') directiveRef: SwiperDirective;
//   index:number;
//   schedule: Schedule;
//   schedules: any;
//   momo:any;
//   public type: string = 'component';
//
//   public config: SwiperConfigInterface = {
//     scrollbar: null,
//     direction: 'horizontal',
//     slidesPerView: 9,
//     scrollbarHide: true,
//     keyboardControl: true,
//     mousewheelControl: true,
//     scrollbarDraggable: true,
//     scrollbarSnapOnRelease: true,
//     pagination: '.swiper-pagination',
//     paginationClickable: true,
//     nextButton: '.swiper-button-next',
//     prevButton: '.swiper-button-prev'
//   };
//
//   constructor( private scheduleService: ScheduleService ) {
//     this.koko();
//     this.config.slidesPerView = 7;
//     this.directiveRef.setIndex(8);
//     // console.log(this.momo);
//   }
//
//   ngOnInit() {
//   }
//   ngAfterViewInit() {
//     // this.directiveRef.setIndex(9);
//
//   }
//   koko() {
//     return this.scheduleService.getSchedules().subscribe((schedules) => {
//           const currentTime = new Date().toLocaleTimeString('en-GB');
//           this.schedules = schedules;
//           this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
//           if(this.index > 0){
//             this.index = this.index-1;
//           }else {
//             this.index = 0;
//           }
//           // this.activeSlideIndex = this.index;
//           //
//           // this.idaho = this.index;
//           // this.scheduleService.addCount(this.idaho);
//           // this.usefulSwiper.swiper.slideTo(this.index);
//           // this.usefulSwiper.params.slideTo(this.index);
//
//         }
//     )
//
//     // console.log(this.index);
//   }
//
//
// }
