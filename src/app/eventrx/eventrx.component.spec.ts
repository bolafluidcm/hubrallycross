import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventrxComponent } from './eventrx.component';

describe('EventrxComponent', () => {
  let component: EventrxComponent;
  let fixture: ComponentFixture<EventrxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventrxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventrxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
