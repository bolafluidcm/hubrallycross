import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Dropdown } from './dropdown';
import { Sidenav } from './sidenav';
import { JsonParseService } from '../services/json-parse.service'
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './../../assets/templates/full-layout.html'
})
export class FullLayoutComponent implements OnInit {
  private rx2home = environment.rx2Home;

  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};

  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  // selectedDropdown: Dropdown = new Dropdown(1, 'SC', 'Supercar', 'home');
  errorMessage: string;
  currentEvent:  any[];
  selectedDropdown: Dropdown;

  dropdowns = [
  new Dropdown(1, 'SC', 'Supercar', 'supercar'),
  new Dropdown(2, 'S1600', 'Super1600', 'super1600'),
  // new Dropdown(3, 'TC', 'Touringcar', 'touringcar'),
  new Dropdown(4, 'RX2', 'RX2', 'rx2'),
  new Dropdown(5, 'ERX', 'Supercar ERX', 'supercar_erx')
  ];
  sidenavs = [
  new Sidenav('Schedule of events', 'schedule_of_events'),
  new Sidenav('Entrants', 'entrants'),
  new Sidenav('Live timings', 'live_timings'),
  new Sidenav('Vidiprinter', 'vidiprinter'),
  new Sidenav('Intermediate points', 'intermediate_points'),
  new Sidenav('Championship points', 'championship_points'),
  new Sidenav('Race documents', 'race_documents')
  ];
  selected: any;
  activeRoute: any;
  toSplitRoute: any;
  parentRoute: string;
  currentParentRoute: string;
  // levels:Array<Object> = [
  //   {num: 0, name: "AA"},
  //   {num: 1, name: "BB"}
  // ];

  constructor(private router: Router, private service: JsonParseService, private route: ActivatedRoute){
    this.router.events.subscribe((res) => {
      this.activeRoute = this.router.url;
      this.toSplitRoute = this.router.url.split(/[/ ]+/).shift();
      this.parentRoute = this.router.url.split('/')[1];
      this.currentParentRoute = this.router.url.split('/')[2];
      // console.log(this.activeRoute,"Active URL");
      // console.log(this.parentRoute,"O URL");
    })

    //!!both methods working
    // router.events.subscribe(event => {
    //   if (event instanceof NavigationEnd) {
    //     this.activeRoute = event.url;
    //     console.log(this.activeRoute ,"Current URL");
    //   }
    // });

  }

  ngOnInit(): void {
    this.selectedDropdown = this.dropdowns[0];
    // this.selected = this.levels[0];
    this.getEvent();
  }

  getEvent() {
  this.service.getEvent()
      .subscribe(
          currentEvent => this.currentEvent = currentEvent,
          error =>  this.errorMessage = <any>error);
}


  oninput($event){
    $event.preventDefault();
  }
  onSelect(){
    // console.log('I am the selected' + this.selectedDropdown);
  }
  // getResult(event){
  //   this.router.navigate(['your_path', event])
  // }
  navigateTo(value) {
    if (value) {
      this.router.navigate([value]);
    }
    return false;
  }


}
