import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import $ from 'jquery';
import 'hammerjs';
import { SwiperModule } from 'angular2-useful-swiper';
import { MaterialModule } from '@angular/material';

import { NgPipesModule } from 'ngx-pipes';
import { GroupByPipe } from './pipes/group-by-pipe';
import { GroupsPipe } from './pipes/groups-pipe';
import { KeysPipe } from './pipes/keys-pipe';
import { ExplodePipe } from './pipes/explode-pipe';
import { TruncatePipe } from './pipes/truncate-pipe';
import { HeatitlePipe } from './pipes/heatitle-pipe';
import { OrdinalPipe } from './pipes/ordinal-pipe';
import { ExplodetimePipe } from './pipes/explodetime-pipe';
import { DateFormatPipe } from './pipes/date-format-pipe';
import { SafehtmlPipe } from './pipes/safehtml-pipe';
import { YoutubePipe } from './pipes/youtube-pipe';

import { TeamsComponent } from 'app/teams/teams.component';
import { NewsComponent } from 'app/news/news.component';
import { NewsListComponent } from 'app/news/news-list/news-list.component';
import { NewsItemComponent } from 'app/news/news-list/news-item/news-item.component';
import { NewsService } from 'app/news/news.service';
import { JsonParseService } from 'app/services/json-parse.service';
import { LoaderService } from 'app/services/loader.service';
import { NewsResolve } from 'app/news/news.resolve';


import { ScheduleComponent } from 'app/schedule/schedule.component';
import { SchedulePageComponent } from 'app/schedule/schedule-page.component';
import { ScheduleService } from 'app/schedule/schedule.service';
import { Now } from 'app/schedule/now';

import { TweetsComponent } from 'app/tweets/tweets.component';
import { TweetsService } from 'app/tweets/tweets.service';

import { ScHeatsComponent } from 'app/race/supercar/heats.component';
import { ScPositionsComponent } from 'app/race/supercar/positions.component';
import { ScIntermediateComponent } from 'app/race/supercar/intermediate.component';
import { ScChampionshipsComponent } from 'app/race/supercar/championships.component';
import { SupercarMainComponent } from 'app/race/supercar/supercar.component';

import { ErxHeatsComponent } from 'app/race/erx/heats.component';
import { ErxPositionsComponent } from 'app/race/erx/positions.component';
import { ErxIntermediateComponent } from 'app/race/erx/intermediate.component';
import { ErxChampionshipsComponent } from 'app/race/erx/championships.component';

import { Rx2HeatsComponent } from 'app/race/rx2/heats.component';
import { Rx2PositionsComponent } from 'app/race/rx2/positions.component';
import { Rx2IntermediateComponent } from 'app/race/rx2/intermediate.component';
import { Rx2ChampionshipsComponent } from 'app/race/rx2/championships.component';
import { Rx2MainComponent } from 'app/race/rx2/rx2.component';

import { Sup16HeatsComponent } from 'app/race/super1600/heats.component';
import { Sup16PositionsComponent } from 'app/race/super1600/positions.component';
import { Sup16IntermediateComponent } from 'app/race/super1600/intermediate.component';
import { Sup16ChampionshipsComponent } from 'app/race/super1600/championships.component';

import {  TourHeatsComponent } from 'app/race/touringcar/heats.component';
import {  TourPositionsComponent } from 'app/race/touringcar/positions.component';
import {  TourIntermediateComponent } from 'app/race/touringcar/intermediate.component';
import {  TourChampionshipsComponent } from 'app/race/touringcar/championships.component';

import { PreraceComponent } from 'app/rxhubmod/prerace/prerace.component';
import { PreraceNewsListComponent } from 'app/rxhubmod/prerace/prerace-news-list.component';
import { PreraceShakedownComponent } from 'app/rxhubmod/prerace/prerace-shakedown.component';

import { ErxComponent } from 'app/race/entrants/erx/erx.component';
import { Rx2Component } from 'app/race/entrants/rx2/rx2.component';
import { ScComponent } from 'app/race/entrants/sc/sc.component';
import { Super1600Component } from 'app/race/entrants/super1600/super1600.component';
import { TourcarComponent } from 'app/race/entrants/tourcar/tourcar.component';

import { IntermediatePageComponent } from 'app/rxhubmod/sunday-evening/components/intermediate-page.component';
import { ChampionshipsPageComponent } from 'app/rxhubmod/sunday-evening/components/championships-page.component';
import { EntrantsPageComponent } from 'app/rxhubmod/sunday-evening/components/entrants-page.component';

import { ErxMainComponent } from 'app/race/erx/erx.component';
import { Super1600MainComponent } from 'app/race/super1600/super1600.component';
import { TouringcarMainComponent } from 'app/race/touringcar/touringcar.component';
import { RaceDocumentsComponent } from 'app/race/race-documents.component';

/*Base Components*/
import { PositionsBaseComponent } from './basecomponent/positions-base.component';
import { HeatBaseComponent } from './basecomponent/heat-base.component';


@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    NgPipesModule,
    RouterModule,
    SwiperModule
  ],
  declarations: [ GroupByPipe, GroupsPipe, KeysPipe, ExplodePipe, TruncatePipe, YoutubePipe, PositionsBaseComponent, HeatBaseComponent,
                 HeatitlePipe, OrdinalPipe, TeamsComponent, ExplodetimePipe, DateFormatPipe, SafehtmlPipe,
                 NewsComponent, NewsListComponent, NewsItemComponent, ScheduleComponent, SchedulePageComponent, TweetsComponent, PreraceComponent,
                PreraceNewsListComponent, PreraceShakedownComponent, RaceDocumentsComponent,
                ScChampionshipsComponent, ScHeatsComponent, ScPositionsComponent, ScIntermediateComponent,
                ErxChampionshipsComponent, ErxHeatsComponent, ErxPositionsComponent, ErxIntermediateComponent,
                Rx2ChampionshipsComponent, Rx2HeatsComponent, Rx2PositionsComponent, Rx2IntermediateComponent,
                Sup16ChampionshipsComponent, Sup16HeatsComponent, Sup16PositionsComponent, Sup16IntermediateComponent,
      TourChampionshipsComponent,  TourHeatsComponent,  TourPositionsComponent,  TourIntermediateComponent,
      ErxComponent, Rx2Component, ScComponent, Super1600Component, TourcarComponent, Now,
      SupercarMainComponent, Rx2MainComponent, IntermediatePageComponent, ChampionshipsPageComponent,EntrantsPageComponent,
      ErxMainComponent, Super1600MainComponent, TouringcarMainComponent

  ],
  providers:    [ JsonParseService, NewsService, NewsResolve, LoaderService ],
  exports:    [ GroupByPipe, GroupsPipe, KeysPipe, ExplodePipe, TruncatePipe, HeatitlePipe, SafehtmlPipe, YoutubePipe, PositionsBaseComponent, HeatBaseComponent,
               OrdinalPipe, TeamsComponent, ExplodetimePipe, DateFormatPipe, NewsComponent,
               NewsListComponent, NewsItemComponent, ScheduleComponent, SchedulePageComponent, TweetsComponent, PreraceComponent,
                PreraceNewsListComponent, PreraceShakedownComponent, RaceDocumentsComponent,
                ScChampionshipsComponent, ScHeatsComponent, ScPositionsComponent, ScIntermediateComponent,
              ErxChampionshipsComponent, ErxHeatsComponent, ErxPositionsComponent, ErxIntermediateComponent,
              Rx2ChampionshipsComponent, Rx2HeatsComponent, Rx2PositionsComponent, Rx2IntermediateComponent,
            Sup16ChampionshipsComponent, Sup16HeatsComponent, Sup16PositionsComponent, Sup16IntermediateComponent,
      TourChampionshipsComponent,  TourHeatsComponent,  TourPositionsComponent,  TourIntermediateComponent,
      ErxComponent, Rx2Component, ScComponent, Super1600Component, TourcarComponent, Now,
      SupercarMainComponent, Rx2MainComponent, IntermediatePageComponent, ChampionshipsPageComponent, EntrantsPageComponent,
      ErxMainComponent, Super1600MainComponent, TouringcarMainComponent
  ]
})
export class SharedModule { }





