import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../services/json-parse.service';
import { ExplodePipe } from '../../shared/pipes/explode-pipe';
import { TruncatePipe } from '../../shared/pipes/truncate-pipe';
import { animateFactory } from 'ng2-animate';
import { environment } from '../../../environments/environment';
import _ from 'lodash';


@Component({
  moduleId: module.id, // this is the key
  selector: 'app-positions-base',
  templateUrl: '../../../assets/templates/positions.html',
  animations: [animateFactory(500, 0, 'ease-in')]

})

export class PositionsBaseComponent implements OnInit {
  public erx = environment.erx;
  flagsUrl: any = environment.flagsUrl;
  manufacturersUrl: any  = environment.manufacturersUrl;
  carsUrls: any  = environment.carsUrls;
  carSmallSize: any  = environment.carSmallSize;
  carLargeSize: any  = environment.carLargeSize;
  carPic;
  show: number = 10;
  showMobile: number = 5;
  next: number = 5;
  hide: boolean = true;
  totals;
  itemLength;
  // items: Array<any>;
  items:any;

  config: Object = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    slidesPerView: 1,
    loop:false
  };
  @ViewChild('loadMore') loadMore:ElementRef;
  constructor(public service: JsonParseService, public renderer: Renderer){
    // const catID = this.erx;
    // service.getPositions(catID).subscribe(
    //   data => {
    //     const grouped = _.groupBy(data, function (d) {
    //       return  d.heat;
    //     });
    //     this.items = grouped;
    //   });

  }

  showMore() {
    let event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.show = this.show + this.next;
    if(this.show > this.items.length){
      this.hide = false;
    }
    this.loadMore.nativeElement.click();
  }

  showMoreMobile() {
    let event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.showMobile = this.showMobile + this.next;
    if(this.showMobile > this.items.length){
      this.hide = false;
    }
    this.loadMore.nativeElement.click();
  }

  setDefaultCarPic() {
    this.carPic = "http://test.rallycrossrx.com/_img/car_missing.png";
  }
  animationStarted(event: AnimationEvent) {
    //console.warn('Animation started: ', event);
  }

  animationDone(event: AnimationEvent) {
    //console.warn('Animation done: ', event);
  }
  ngOnInit() {}

}
