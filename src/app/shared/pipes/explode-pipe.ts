import { Pipe, PipeTransform } from '@angular/core';
//import { InvalidPipeArgumentException } from '@angular2/src/common/pipes/invalid_pipe_argument_exception';
//import { isString, isBlank } from '@angular2/src/facade/lang';

@Pipe({name: 'explode'})
export class ExplodePipe implements PipeTransform {
    //transform(map: {}, args: any[] = null): any {
    //    if (!map)
    //        return null;
    //    return Object.keys(map)
    //        .map((key) => ({ 'key': key, 'value': map[key] }));
    //}
  transform(input: string, separator: string,index:number): string {
    return input.split(separator)[index];
  }
}