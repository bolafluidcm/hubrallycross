import { Component, OnInit, OnDestroy } from '@angular/core';
import { ScheduleService } from './schedule.service';
import { Routes, RouterModule, Router } from '@angular/router';
import { Schedule } from './schedule';
import { Now } from './now';
import { JsonParseService } from '../services/json-parse.service'


@Component({
    selector: 'app-schedule-page',
    templateUrl: './schedule-page.component.html',
    providers: [ ScheduleService ],
    styles: []
})
export class SchedulePageComponent implements OnInit {
    errorMessage: string;
    // schedules: any[];
    schedules: Schedule;
    mode = 'Observable';
    currentEvent:  any[];

    interval;
    now = new Date();
    dateString: boolean;
    dayNumber = new Date().getDay();
    asiko: string;
    selectedIndex: number;
    index: number = 9;
    myslider: any;
    config: Object = {
        pagination: '.swiper-pagination',
        // nextButton: '.swiper-button-next',
        // prevButton: '.swiper-button-prev',
        spaceBetween: 10,
        slidesPerView: 4,
        paginationClickable: true,
        loop:false,
        initialSlide: 9,
        runCallbacksOnInit: true,
        // autoplay: 2500,
        // autoplayDisableOnInteraction: true,
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            737: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    };

    constructor(private scheduleService: ScheduleService, private _router: Router,  private service: JsonParseService ) {
        this.interval = setInterval(()=> {
            this.now = new Date() ;
        })
      // console.log('today is ' + this.dayNumber);
        this.selectedIndex = 0;

        const currentTime = new Date().toTimeString().substr(0,8);
        this.asiko = new Date().toTimeString().substr(0,8);
        console.log('time na ' + this.asiko);
        // console.log('Index mi nko ' + this.selectedIndex);
      // this.dateString = this.now > this.schedules.theTime;
        this.getEvent();
    }

    ngOnInit() {
        this.getSchedules();
        // this.myslider.slideTo(9,0,false);

        // console.log('schedule time is ' + this.schedules.theTime);
    }
    ngOnDestroy(){
        clearInterval(this.interval)
    }

    getSchedules() {
        this.scheduleService.getSchedulesPage()
            .subscribe(
                schedules => this.schedules = schedules,
                error =>  this.errorMessage = <any>error);

    }

    next() {
        ++this.selectedIndex;
    }

    previous() {
        --this.selectedIndex;
    }
    getEvent() {
        this.service.getEvent()
            .subscribe(
                currentEvent => this.currentEvent = currentEvent,
                error =>  this.errorMessage = <any>error);
    }
}
