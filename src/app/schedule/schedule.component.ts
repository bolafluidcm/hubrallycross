import { Component, OnInit, OnDestroy, ViewChild, Input, Renderer, ElementRef } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { ScheduleService } from './schedule.service';
import { JsonParseService } from '../services/json-parse.service'
// import { SwiperComponent } from 'angular2-useful-swiper';
import { Schedule } from './schedule';
import { ScheduleIndex } from './schedule-index';
import _ from 'lodash';

declare var Swiper: any;

@Component({
    selector: 'app-schedule',
    templateUrl: './schedule.component.html',
    providers: [ ScheduleService ],
    styles: [],
})
export class ScheduleComponent implements OnInit {
    // @ViewChild('usefulSwiper')
    //     set usefulSwiper(ref: any){
    //     console.log(ref);
    // }
    // @ViewChild('usefulSwiper') usefulSwiper: SwiperComponent;
    // swiper: Swiper;
    @Input() src: string;
    // @ViewChild('player') player;
    // @ViewChild('ptag') ptag;
    @ViewChild("ptag", {read: ElementRef}) ptag: ElementRef;

    public idaho: number;
    errorMessage: string;
    schedule: Schedule;
    schedules: Schedule[];
    mode = 'Observable';
    now = new Date();
    countDown;
    counter = 60;
    index: number;
    eva:any;
    scheduleIndex : ScheduleIndex;
    currentEvent:  any[];
    config: any = {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 0,
        slidesPerView: 10,
        paginationClickable: false,
        loop:false,
        // initialSlide: 3,
        // runCallbacksOnInit: true,
        // autoplay: 2500,
        // autoplayDisableOnInteraction: true,
        breakpoints: {
            1024: {
                slidesPerView: 7,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 6,
                spaceBetween: 10
            },
            737: {
                slidesPerView: 6,
                spaceBetween: 20
            },
            640: {
                slidesPerView: 6,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 4,
                spaceBetween: 10
            }
        }
    };

    constructor(
        private scheduleService: ScheduleService, 
        private _router: Router,  
        private service: JsonParseService,
        private renderer: Renderer,
        private el: ElementRef
        ) {
        // this.activateSlide();
        // this.scheduleService.getSchedules().subscribe((schedules) => {
        //         const currentTime = new Date().toLocaleTimeString('en-GB');
        //         this.schedules = schedules;
        //         this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
        //         if(this.index > 0){
        //             this.index = this.index-1;
        //         }else {
        //             this.index = 0;
        //         }
        //         // this.countDown = Observable.timer(0,1000)
        //         // .take(this.counter)
        //         // .map(()=> --this.counter);
        //
        //         // console.log(this.schedules[this.index]['theTime']);
        //         // console.log(this.schedules[this.index-1]['theTime']);
        //         // console.log(this.schedules[this.index]['diffTime'] - this.schedules[this.index-1]['diffTime']);
        //
        //         this.scheduleService.getSchedule(335)
        //             .subscribe((schedule) => {
        //                 this.schedule = schedule;
        //
        //                 // console.log(this.schedule);
        //             }
        //         )
        //
        //
        //         this.idaho = this.index;
        //         this.scheduleService.addCount(this.idaho);
        //         // let scheduleIndex = new ScheduleIndex();
        //         // scheduleIndex.idx = this.index;
        //         // console.log(this.idaho);
        //         // let scheduleIndex = this.idaho;
        //         // this.scheduleService.getScheduleIndex(this.idaho);
        //
        // }
        // )
        this.getEvent();
    }

    ngOnInit() {
        let eva = this.getCount();
        // console.log(eva);
    }

     ngAfterViewInit(){
         this.mainSlide();
         // this.usefulSwiper.swiper.slideTo(3);
             // console.log('result is ' + this.usefulSwiper.swiper.activeIndex);
             // console.log(this.scheduleIndex);
         // console.log(this.idaho);
         // console.log(this.player);

         // console.log(this.ptag);
         // console.log(this.ptag.nativeElement.baseURI);
         // console.log(this.ptag.nativeElement.outerHTML);
         // this.eva = this.getCount();

     }
    ngAfterContentInit(){
        // console.log(this.ptag);

        // const bola  =  _.parseInt(this.getCount());
        // console.log(bola);

    }

    ngOnDestroy(){
    }
 
    getSchedules() {
        this.scheduleService.getSchedules()
            .subscribe(
                schedules => this.schedules = schedules,
                error =>  this.errorMessage = <any>error);

    }

    getCount() {
        this.eva = this.scheduleService.get();

        return this.eva;
    }

    mainSlide() {
        this.scheduleService.getSchedules().subscribe((schedules) => {
                const currentTime = new Date().toLocaleTimeString('en-GB');
                this.schedules = schedules;
                this.index = _.parseInt(this.schedules.findIndex(x => x.theTime >= currentTime));
                if(this.index > 0){
                    this.index = this.index-1;
                }else {
                    this.index = 0;
                }

                // this.usefulSwiper.swiper.slideTo(this.index);
                // this.config.initialSlide = this.index;

            }
        )

        // console.log(this.index);
    }
    getEvent() {
        this.service.getEvent()
            .subscribe(
                currentEvent => this.currentEvent = currentEvent,
                error =>  this.errorMessage = <any>error);
    }


}
