import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Schedule } from './schedule';
import { ScheduleIndex } from './schedule-index';

@Injectable()
export class ScheduleService {
schedules: Schedule[];
schedule: Schedule;
scheduleIndex: ScheduleIndex;
public count = 0;
    // private eventsUrl = 'http://api.rallycrossrx.com/api/schedule/49';
    private eventsUrl = 'http://api.rallycrossrx.com/api/schedule/49';  // URL to web API

    constructor (private http: Http) {}

    // getSchedules() {
    //     return this.http.get(this.eventsUrl)
    //         .map((res:Response) => res.json())
    //         //errors if any
    //         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    // }



    get() {
        return this.count;
    }
    addCount(count) {
        this.count = count;
    }
    getSchedulesPage() {
        return this.http.get(this.eventsUrl)
            .map((response: Response) => response.json());
    }

    getSchedules(): Observable<Schedule[]> {
        return this.http.get(this.eventsUrl)
            .map((response: Response) => response.json());
    }


    getSchedule(id: number): Observable<Schedule> {
        return this.getSchedules()
            .map(schedules => schedules.find(schedule => schedule.id == id));
    }
    getScheduleByTime(id: number): Observable<Schedule> {
        return this.getSchedules()
            .map(schedules => schedules.find(schedule => schedule.id == id));
    }

    getScheduleIndex(data){
        // .map(data => this.scheduleIndex)
        this.scheduleIndex.idx = data;
    }

    getRxEVents() : Observable<any> {
        return this.http.get(this.eventsUrl )
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

}




