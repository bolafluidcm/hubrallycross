import { Component, OnInit } from '@angular/core';
import { JsonParseService } from '../../../services/json-parse.service';
import { Entrant } from '../entrant';
import _ from 'lodash';
import $ from 'jquery';


@Component({
  selector: 'app-sc',
  templateUrl: './sc.component.html',
  styleUrls: ['./sc.component.scss']
})

export class ScComponent implements OnInit {
  // items: Array<any>;
  // grouped:any;
  items: any;
  // items: any = _;
    config: Object = {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 10,
        slidesPerView: 1,
        loop:false
    };

  constructor(private service: JsonParseService) {
    // service.getEntrants().subscribe(p=>this.items = p);
    service.getEntrants().subscribe(
        data => {
          const grouped = _.groupBy(data, 'category');
          this.items = grouped;
          // console.log(this.items);
        }
  );
    // const grouped = _.groupBy(this.items, 'category');
    // console.log(grouped);

    // console.log('show entrants ' + this.items);


    // this.service.getEntrants()
    //     .subscribe(
    //         data => {
    //           const myArray = [];
    //           for (let key in data) {
    //             myArray.push(data[key]);
    //           }
    //           this.items = myArray;
    //           console.log('show entrants ' + this.items);
    //
    //         }
    //     );

  }

  ngOnInit() {
  }

}
