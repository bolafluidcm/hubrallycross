export interface Entrant {
    position: string;
    name: string;
    name_short: string;
    flag: string;
    category: string;
    cat_short: string;
}