import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren, Inject } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../services/json-parse.service';
import { animateFactory } from 'ng2-animate';
import { environment } from '../../../environments/environment';
import _ from 'lodash';
import { PositionsBaseComponent } from '../../shared/basecomponent/positions-base.component';

@Component({
  selector: 'app-sup16-positions',
  templateUrl: '../../../assets/templates/positions.html',
  animations: [animateFactory(500, 0, 'ease-in')]

})

export class Sup16PositionsComponent extends PositionsBaseComponent {
  public super1600 = environment.super1600;

  @ViewChild('loadMore') loadMore:ElementRef;
  constructor(@Inject(JsonParseService) service: JsonParseService, @Inject(Renderer) renderer: Renderer){
    super( service, renderer );
    const catID = this.super1600;
    this.service.getPositions(catID).subscribe(
      data => {
        const grouped = _.groupBy(data, function (d) {
          return  d.heat;
        });
        const ordered = {};
        Object.keys(grouped).sort().forEach(function(key) {
          ordered[key] = grouped[key];
        });
        this.items = ordered;
      });

  }


  ngOnInit() {}

}
