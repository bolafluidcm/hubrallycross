export interface Heat {
    heat: string;
    Race_Number: number;
    Position: string;
    Time_Total: number;
    country: string;
    driver_name: string;
    driver_name_short: string;
    car: string;
    badge: string;
    newTime: number;
    // entrants: Entrant[];
}

// export interface Entrant {
//     position: number;
//     driver: string;
//     country: string;
//     car: string;
//     badge: string;
//     totaltime: number;
// }