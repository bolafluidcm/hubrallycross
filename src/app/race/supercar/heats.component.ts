import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { JsonParseService } from '../../services/json-parse.service';
import $ from 'jquery';
import { Heat } from '../heat';
import _ from 'lodash';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-heats',
  templateUrl: '../../../assets/templates/newheats.html',
  // templateUrl: './se_heats.html',
  styles: []
})
export class ScHeatsComponent implements OnInit {
    flagsUrl: any = environment.flagsUrl;
    manufacturersUrl: any  = environment.manufacturersUrl;
    carsUrls: any  = environment.carsUrls;
    carSmallSize: any  = environment.carSmallSize;
    carLargeSize: any  = environment.carLargeSize;

    items: any[];
	subNewItems: any[];
    heat2: any[] = [];
	heat3: any[] = [];

    config: Object = {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 10,
			slidesPerView: 1,
			loop:false
    };

	constructor(private service: JsonParseService, private el: ElementRef){
        service.getSupCarAll().subscribe(
            data => {
                const grouped = _.groupBy(data, function (d) {
                    return d.heat + "-" + d.Race_Number;
                });
                this.items = grouped;
                // const regrouped = _.sortBy(data, ['Position']);
                // this.subNewItems = regrouped;
                // console.log(this.subNewItems);

                // const groupedItems = _.groupBy(grouped, 'Race_Number');
                // this.subNewItems = groupedItems;
                // console.log('subItems are: ' + groupedItems);
            });



	}

  ngOnInit() {}

}
