import { Component, OnInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { JsonParseService } from '../../services/json-parse.service';
import $ from 'jquery';
import { Heat } from '../heat';
import _ from 'lodash';

@Component({
  selector: 'app-touringcar-heats',
  templateUrl: '../../../assets/templates/newheats.html',
  styles: []
})
export class  TourHeatsComponent implements OnInit {
	//heat: Heat[] = [];
	  items: any;
		// heat2: any[] = [];
		// heat3: any[] = [];

    config: Object = {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 10,
            slidesPerView: 1,
            loop:false
        };

	constructor(private service: JsonParseService, private el: ElementRef){

        service.getTourCHeatsAll().subscribe(
            data => {
                const grouped = _.groupBy(data, function (d) {
                    return d.heat + "-" + d.Race_Number;
                });
                this.items = grouped;
            });
    // this.service.getTourCHeats()
    //   .subscribe(
    //     data => {
    //       const myArray = [];
    //       for (let key in data) {
    //         myArray.push(data[key]);
    //       }
    //       this.items = myArray;
    //     }
    //   );
    //
    // this.service.getTourCHeat2()
    //   .subscribe(
    //     data => {
    //       const myArray = [];
    //       for (let key in data) {
    //         myArray.push(data[key]);
    //       }
    //       this.heat2 = myArray;
    //     }
    //   );
    //
    //
    // this.service.getTourCHeat3()
    //   .subscribe(
    //     data => {
    //       const myArray = [];
    //       for (let key in data) {
    //         myArray.push(data[key]);
    //       }
    //       this.heat3 = myArray;
    //     }
    //   );


	}

  ngOnInit() {}


}
