import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../services/json-parse.service';
import { ExplodePipe } from '../../shared/pipes/explode-pipe';
import { TruncatePipe } from '../../shared/pipes/truncate-pipe';
import { animateFactory } from 'ng2-animate';



@Component({
  selector: 'app-touringcar-intermediate',
  templateUrl: '../../../assets/templates/intermediate.html',
  styles: [],
  animations: [animateFactory(1000, 0, 'ease-in')]  

})
export class  TourIntermediateComponent implements OnInit {

	show: number = 10;
	showMobile: number = 5;
  next: number = 1;
  hide: boolean = true;
  hideMobile: boolean = true;
	data: Observable<Array<any>>;
	items;
	@ViewChild('loadMore') loadMore:ElementRef;
  
	constructor(private service: JsonParseService, private renderer: Renderer){
		service.getTourCIntermediates().subscribe(p=>this.items = p);
	}
		showMore() {
			let event = new MouseEvent('click', {bubbles: true});
			event.stopPropagation();
			this.show = this.show + this.next;
			if(this.show > this.items.length){
				this.hide = false;
			}
			this.loadMore.nativeElement.dispatchEvent(event);
		}
    
		showMoreMobile() {
			let event = new MouseEvent('click', {bubbles: true});
			event.stopPropagation();
			this.showMobile = this.showMobile + this.next;
      if(this.showMobile > this.items.length){
			this.hideMobile = false;    
      }
			this.loadMore.nativeElement.click();
		}
    
		animationStarted(event: AnimationEvent) {
			console.warn('Animation started: ', event);
		}
	
		animationDone(event: AnimationEvent) {
			console.warn('Animation done: ', event);
		}	
  ngOnInit() {}

}
