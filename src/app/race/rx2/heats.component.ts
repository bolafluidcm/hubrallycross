import { Component, OnInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import $ from 'jquery';
import { JsonParseService } from '../../services/json-parse.service';
import _ from 'lodash';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-rx2-heats',
  templateUrl: '../../../assets/templates/newheats.html',
  styles: []
})
export class Rx2HeatsComponent implements OnInit {
    flagsUrl: any = environment.flagsUrl;
    manufacturersUrl: any  = environment.manufacturersUrl;
    carsUrls: any  = environment.carsUrls;
    carSmallSize: any  = environment.carSmallSize;
    carLargeSize: any  = environment.carLargeSize;
    items: any;
		// heat2: any[] = [];
		// heat3: any[] = [];

    config: Object = {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            spaceBetween: 10,
            slidesPerView: 1,
            loop:false
        };

	constructor(private service: JsonParseService, private el: ElementRef){
        service.getRx2HeatsAll().subscribe(
            data => {
                const grouped = _.groupBy(data, function (d) {
                    return d.heat + "-" + d.Race_Number;
                });
                this.items = grouped;
            });
    // this.service.getRx2Heats()
    //   .subscribe(
    //     data => {
    //       const myArray = [];
    //       for (let key in data) {
    //         myArray.push(data[key]);
    //       }
    //       this.items = myArray;
    //     }
    //   );
    //
    // this.service.getRx2Heat2()
    //   .subscribe(
    //     data => {
    //       const myArray = [];
    //       for (let key in data) {
    //         myArray.push(data[key]);
    //       }
    //       this.heat2 = myArray;
    //     }
    //   );
    //
    //
    // this.service.getRx2Heat3()
    //   .subscribe(
    //     data => {
    //       const myArray = [];
    //       for (let key in data) {
    //         myArray.push(data[key]);
    //       }
    //       this.heat3 = myArray;
    //     }
    //   );


	}

  ngOnInit() {}

}
