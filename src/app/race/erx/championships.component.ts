import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../services/json-parse.service';
import { TruncatePipe } from '../../shared/pipes/truncate-pipe';
import { animateFactory } from 'ng2-animate';
import { environment } from '../../../environments/environment';

@Component({
  moduleId: module.id,
  selector: 'app-erx-championships',
  templateUrl: '../../../assets/templates/championships.html',
  styles: [],
  animations: [animateFactory(500, 0, 'ease-in')]
})
export class ErxChampionshipsComponent implements OnInit {
  private erx = environment.erx;
  flagsUrl: any = environment.flagsUrl;
  manufacturersUrl: any  = environment.manufacturersUrl;
  carsUrls: any  = environment.carsUrls;
  carSmallSize: any  = environment.carSmallSize;
  carLargeSize: any  = environment.carLargeSize;
  show: number = 10;
  showMobile: number = 5;
  next: number = 5;
  hide: boolean = true;
  totals;
  itemLength;
  items: Array<any>;
  @ViewChild('loadMore') loadMore:ElementRef;

    data: Observable<Array<any>>;

	constructor(private service: JsonParseService){
		// this.data = service.getChamps();
        const catid = this.erx;
        service.getChamps(catid).subscribe(p=>this.items = p);

	}


    showMore() {
        let event = new MouseEvent('click', {bubbles: true});
        event.stopPropagation();
        this.show = this.show + this.next;
        if(this.show > this.items.length){
            this.hide = false;
        }
        this.loadMore.nativeElement.click();
        // console.log('shown : ' + this.show );
        //this.loadMore.nativeElement.dispatchEvent(event);
    }
    showMoreMobile() {
        let event = new MouseEvent('click', {bubbles: true});
        event.stopPropagation();
        this.showMobile = this.showMobile + this.next;
        if(this.showMobile > this.items.length){
            this.hide = false;
        }
        this.loadMore.nativeElement.click();
    }

    animationStarted(event: AnimationEvent) {
        //console.warn('Animation started: ', event);
    }

    animationDone(event: AnimationEvent) {
        //console.warn('Animation done: ', event);
    }


    ngOnInit() {}
}
