import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Heat } from 'app/race/heat';

const HEAT : Heat[] = [];

@Injectable()
export class NewParseService{
  private baseUrl: string = 'http://datatree.fluidaa4.dyndns.info/api';

  constructor(private http : Http){}

  // getAll(): Observable<Heat[]>{
  //   let heat$ = this.http
  //     .get(`${this.baseUrl}/noob/1/94/H1`, { headers: this.getHeaders()})
  //     .map(mapHeats);
  //     return heat$;
  // }

  private getHeaders(){
    // I included these headers because otherwise FireFox
    // will request text/html
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }
  //get(id: number): Observable<Person> {
  //  let person$ = this.http
  //    .get(`${this.baseUrl}/people/${id}`, {headers: this.getHeaders()})
  //    .map(mapPerson);
  //    return person$;
  //}

  //save(person: Person) : Observable<Response>{
  //  // this won't actually work because the StarWars API
  //  // is read-only. But it would look like this:
  //  return this
  //    .http
  //    .put(`${this.baseUrl}/people/${person.id}`,
  //          JSON.stringify(person),
  //          {headers: this.getHeaders()});
  //}

}


// function mapHeats(response:Response): Heat[]{
//   // The response of the API has a results
//   // property with the actual results
//   return response.json().results.map(toHeat)
// }

// function toHeat(r:any): Heat{
//   let heat = <Heat>({
//     heat: r.heat,
//     race: r.race,
//     positions: r.positions,
//     drivers: r.drivers,
//     countries: r.countries,
//     cars: r.cars,
//     badges: r.badges,
//     totaltimes: r.totaltimes,
//     entrants: r.entrants,
//     position: r.entrants.position,
//     driver: r.entrants.driver,
//     country: r.entrants.country,
//     car: r.entrants.car,
//     badge: r.entrants.badge,
//     totaltime: r.entrants.totaltime,

//   });
//   console.log('Parsed heat:', heat);
//   return heat;
// }

// to avoid breaking the rest of our app
// I extract the id from the person url
//function extractId(personData:any){
//  let extractedId = personData.url.replace('http://swapi.co/api/people/','').replace('/','');
//  return parseInt(extractedId);
//}

// function mapHeat(response:Response): Heat{
//    // toPerson looks just like in the previous example
//    return toHeat(response.json());
// }
