import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Heat } from 'app/race/heat';
import { environment } from '../../environments/environment';

@Injectable()
export class JsonParseService {
    // private apiUrl = 'http://api.rallycrossrx.com/api/';
    private apiUrl = environment.apiEndpoint;
    private currentEvent = environment.currentEvent;
    private currentYear = environment.currentYear;
    private heatsUrl = environment.heatsUrl;
    private supercar = environment.supercar;
    private super1600 = environment.super1600;
    private touringcar = environment.touringcar;
    private rx2 = environment.rx2;
    private erx = environment.erx;
    private entrantsUrl = environment.entrantsUrl;
    heat: Heat[] = [];

    constructor(private http: Http) { }

    //SuperCar

    getSupCarAll() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.supercar + '/' + this.currentEvent)
            .map((response: Response) => response.json());
    }
    // getSupCarHeat1() {
    //     return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.supercar + '/' + this.currentEvent + '/H1')
    //         .map((response: Response) => response.json());
    // }
    //
    //
    // getSupCarHeat2() {
    //     return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.supercar + '/' + this.currentEvent + '/H2')
    //         .map((response: Response) => response.json());
    // }
    //
    // getSupCarHeat3() {
    //     return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.supercar + '/' + this.currentEvent + '/H3')
    //         .map((response: Response) => response.json());
    // }
    //
    //
    // getSupCarHeat4() {
    //     return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.supercar + '/' + this.currentEvent + '/H4')
    //         .map((response: Response) => response.json());
    // }


    private handleError (error: any) {
        console.log(error);
        return Observable.throw(error.json());
    }



    getPositions(catID) : Observable<any> {
        return this.http.get(this.apiUrl + 'heatpositions/' + catID + '/' + this.currentEvent)
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    //
    getIntermediates() : Observable<any> {
        return this.http.get(this.apiUrl + 'intermediate/1/' + this.currentEvent + '')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getIntermediatesPage(koko) : Observable<any> {
        return this.http.get(this.apiUrl + 'intermediate/' + koko + '/' + this.currentEvent)
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }


    getTeams() : Observable<any> {
        return this.http.get(this.apiUrl)
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getChampsPage(catId) : Observable<any> {
        return this.http.get(this.apiUrl + 'championships/' + catId + '/' + this.currentYear)
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getChamps(catId) : Observable<any> {
        return this.http.get(this.apiUrl + 'championships/' + catId + '/' + this.currentYear)
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }


//Super1600 Only

    getSup16All() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.super1600 + '/' + this.currentEvent)
            .map((response: Response) => response.json());
    }

    getSup16Heat2() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.super1600 + '/' + this.currentEvent + '/H2')
            .map((response: Response) => response.json());
    }

    getSup16Heat3() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.super1600 + '/' + this.currentEvent + '/H3')
            .map((response: Response) => response.json());
    }


    getSup16Heat4() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.super1600 + '/' + this.currentEvent + '/H4')
            .map((response: Response) => response.json());
    }

    getSup16Positions() : Observable<any> {
        return this.http.get(this.apiUrl + 'heatpositions/' + this.super1600 + '/' + this.currentEvent + '/H1')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getSup16Intermediates() : Observable<any> {
        return this.http.get(this.apiUrl + 'intermediate/' + this.super1600 + '/' + this.currentEvent + '')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }


//Rx2 Only

    getRx2HeatsAll() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.rx2 + '/' + this.currentEvent)
            .map((response: Response) => response.json());
    }

    getRx2Heat2() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.rx2 + '/' + this.currentEvent + '/H2')
            .map((response: Response) => response.json());
    }


    getRx2Heat3() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.rx2 + '/' + this.currentEvent + '/H3')
            .map((response: Response) => response.json());
    }
    getRx2Heat4() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.rx2 + '/' + this.currentEvent + '/H4')
            .map((response: Response) => response.json());
    }

    getRx2Positions() : Observable<any> {
        return this.http.get(this.apiUrl + 'heatpositions/' + this.rx2 + '/' + this.currentEvent + '/H1')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getRx2Intermediates() : Observable<any> {
        return this.http.get(this.apiUrl + 'intermediate/' + this.rx2 + '/' + this.currentEvent + '')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }


//ERX

    getErxHeatsAll() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.erx + '/' + this.currentEvent)
            .map((response: Response) => response.json());
    }

    getErxHeat2() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.erx + '/' + this.currentEvent + '/H2')
            .map((response: Response) => response.json());
    }


    getErxHeat3() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.erx + '/' + this.currentEvent + '/H3')
            .map((response: Response) => response.json());
    }
    getErxHeat4() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.erx + '/' + this.currentEvent + '/H4')
            .map((response: Response) => response.json());
    }


    getErxPositions() : Observable<any> {
        return this.http.get(this.apiUrl + 'heatpositions/' + this.erx + '/' + this.currentEvent + '/H1')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getErxIntermediates() : Observable<any> {
        return this.http.get(this.apiUrl + 'intermediate/' + this.erx + '/' + this.currentEvent + '')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }



//Touring Car

    getTourCHeatsAll() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.touringcar + '/' + this.currentEvent)
            .map((response: Response) => response.json());
    }

    getTourCHeat2() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.touringcar + '/' + this.currentEvent + '/H2')
            .map((response: Response) => response.json());
    }


    getTourCHeat3() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.touringcar + '/' + this.currentEvent + '/H3')
            .map((response: Response) => response.json());
    }
    getTourCHeat4() {
        return this.http.get(this.apiUrl + this.heatsUrl + '/' + this.touringcar + '/' + this.currentEvent + '/H4')
            .map((response: Response) => response.json());
    }


    getTourCPositions() : Observable<any> {
        return this.http.get(this.apiUrl + 'heatpositions/' + this.touringcar + '/' + this.currentEvent + '/H1')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    getTourCIntermediates() : Observable<any> {
        return this.http.get(this.apiUrl + 'intermediate/' + this.touringcar + '/' + this.currentEvent + '')
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }


//Categories
    getCat() {
        return this.http.get(this.apiUrl + 'categories')
            .map((response: Response) => response.json());
    }
//Event
    getEvent() {
        return this.http.get(this.apiUrl + 'event' + '/' + this.currentEvent)
            .map((response: Response) => response.json());
    }
//Entrants

    getEntrantsPage(catid) : Observable<any> {
        return this.http.get(this.apiUrl + 'entrantpage/' + catid + '/' + this.currentEvent )
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    getEntrants() : Observable<any> {
        return this.http.get(this.apiUrl + this.entrantsUrl + '/' + this.currentEvent )
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }



}
