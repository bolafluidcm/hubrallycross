import { Component, OnInit } from '@angular/core';
// import {Driver} from '../../drivers/driver';
import { Observable } from 'rxjs/Rx';
import { JsonParseService } from './../services/json-parse.service';

@Component({
selector: 'app-teams',
  templateUrl: './../../assets/templates/teams.html',
styles: []
})
export class TeamsComponent implements OnInit {
  ngOnInit(): void {
  }

  data: Observable<Array<any>>;

  constructor(private service: JsonParseService){
    this.data = service.getTeams();
    console.log('AppComponent.data:' + this.data);
  }


}
