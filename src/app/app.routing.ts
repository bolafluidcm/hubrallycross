import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { DriversComponent } from './drivers/drivers.component';
import { NewsComponent } from 'app/news/news.component';
import { NewsListComponent } from 'app/news/news-list/news-list.component';
import { NewsItemComponent } from 'app/news/news-list/news-item/news-item.component';
import { NewsResolve } from 'app/news/news.resolve';
import { AuthGuardService } from 'app/shared/auth-guard.service';
import { SupercarMainComponent } from 'app/race/supercar/supercar.component';
import { IntermediatePageComponent } from 'app/rxhubmod/sunday-evening/components/intermediate-page.component';


export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'home',
        loadChildren: './rxhubmod/home/home.module#HomeModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'super1600',
        loadChildren: './rxhubmod/supsix/supsix.module#SupsixModule'
      },
      {
        path: 'rx2',
        loadChildren: './rxhubmod/rx2/rx2.module#Rx2Module'
      },
      {
        path: 'supercar_erx',
        loadChildren: './rxhubmod/erx/erx.module#ErxModule'
      },
      {
        path: 'touringcar',
        loadChildren: './rxhubmod/touringcar/touringcar.module#TouringcarModule'
      },
      {
        path: 'news',
        component: NewsComponent,
      },
      {
        path: 'news/:id',
        component: NewsItemComponent
      },
      {
        path: 'prerace',
        loadChildren: './rxhubmod/prerace/prerace.module#PreraceModule'
      },      
      {
        path: 'sunday_evening',
        loadChildren: './rxhubmod/sunday-evening/sunday-evening.module#SundayEveningModule'
      },
      {
        path: 'supercar',
        component: SupercarMainComponent
      },
      {
        path: 'intermediate_points',
        component: IntermediatePageComponent
      }
      // ,
      // {
      //   path: 'supercar',
      //   loadChildren: './supermodules/supercar/supercar.module#SupercarModule'
      // }
    ]
  }

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

//export const appRoutingProviders: any[] = [
//
//];

export class AppRoutingModule {}
//export const routing = RouterModule.forRoot(routes);
