import { Component, OnInit } from '@angular/core';
import { TweetsService } from './tweets.service';
import { Routes, RouterModule, Router } from '@angular/router';


@Component({
    selector: 'app-tweets',
    templateUrl: './tweets.component.html',
    providers: [ TweetsService ],
    styles: []
})
export class TweetsComponent implements OnInit {
    errorMessage: string;
    tweets: any[];
    mode = 'Observable';
    //router;



    config: Object = {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 10,
        slidesPerView: 4,
        paginationClickable: true,
        loop:true,
        autoplay: 2500,
        autoplayDisableOnInteraction: true,
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            320: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    };

    constructor(private tweetsService: TweetsService, private _router: Router ) { }

    ngOnInit() {
        this.getTweets();
    }


    getTweets() {
        this.tweetsService.getTweets()
            .subscribe(
              tweets => this.tweets = tweets,
                error =>  this.errorMessage = <any>error);
    }



}
