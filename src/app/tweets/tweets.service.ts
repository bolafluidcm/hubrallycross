import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class TweetsService {

    private tweetsUrl = 'http://api.rallycrossrx.com/api/tweets';  // URL to web API

    constructor (private http: Http) {}

    getTweets(): Observable<any[]> {
        return this.http.get(this.tweetsUrl)
            .map((res:Response) => res.json())
            //errors if any
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

}




