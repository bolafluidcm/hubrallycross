import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { JsonParseService } from '../../../app/services/json-parse.service';
import { NewParseService } from '../../../app/services/newparse.service';
import { SupercarComponent } from './supercar.component';
import { BottomVideosComponent } from './videos/bottom-videos.component';
import { TopVideosComponent } from './videos/top-videos.component';
import { SwiperModule } from 'angular2-useful-swiper';
import { SupercarRoutingModule } from './supercar-routing.module';
import { SharedModule } from '../../../app/shared/shared.module';
import { NgPipesModule } from 'ngx-pipes';
import { FullLayoutComponent } from './components/full-layout.component';
import { IntermediatePageComponent } from './components/intermediate-page.component';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    SupercarRoutingModule,
    SharedModule,
    NgPipesModule,
    SwiperModule
  ],
  declarations: [ SupercarComponent, BottomVideosComponent, TopVideosComponent, FullLayoutComponent, IntermediatePageComponent
  ],
  providers:    [ JsonParseService, NewParseService ]
})
export class SupercarModule { }





