import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../../services/json-parse.service';
import { ExplodePipe } from '../../../shared/pipes/explode-pipe';
import { TruncatePipe } from '../../../shared/pipes/truncate-pipe';
import { animateFactory } from 'ng2-animate';



@Component({
  selector: 'app-intermediate-page',
  templateUrl: '../layouts/intermediate-page.html',
  styles: [],
  animations: [animateFactory(1000, 0, 'ease-in')]  
  //animations: [
  //trigger('flyInOut', [
  //  state('in', style({transform: 'translateX(0)'})),
  //  transition('void => *', [
  //    style({transform: 'translateX(-100%)'}),
  //    animate(600)
  //  ]),
  //  transition('* => void', [
  //    animate(300, style({transform: 'translateX(100%)'}))
  //  ])
  //]),
  //  trigger('itemAnim', [
  //    transition(':enter', [
  //      style({transform: 'translateX(-100%)'}),
  //      animate(350)
  //    ]),
  //    transition(':leave', [
  //      group([
  //        animate('0.2s ease', style({
  //          transform: 'translate(150px,25px)'
  //        })),
  //        animate('0.5s 0.2s ease', style({
  //          opacity: 0
  //        }))
  //      ])
  //    ])
  //  ]),
  //trigger('shrinkOut', [
  //  state('in', style({height: '*'})),
  //  transition('* => void', [
  //    style({height: '*'}),
  //    animate(6000, style({height: 0}))
  //  ])
  //])    
  //  
  //]	
})
export class IntermediatePageComponent implements OnInit {

	show: number = 10;
	showMobile: number = 5;
  next: number = 5;
  hide: boolean = true;
  hideMobile: boolean = true;
	data: Observable<Array<any>>;
	items;
	@ViewChild('loadMore') loadMore:ElementRef;
  
	constructor(private service: JsonParseService, private renderer: Renderer){
		//this.data = service.getIntermediates
		service.getIntermediates().subscribe(p=>this.items = p);
		//console.log("Intermediate:" + this.data);
		//this.clicked(event){
		//	
		//}
	}
		showMore() {
			let event = new MouseEvent('click', {bubbles: true});
			event.stopPropagation();
			this.show = this.show + this.next;
			if(this.show > this.items.length){
				this.hide = false;
			}
			//this.clickme.nativeElement.click();
			//this.renderer.invokeElementMethod(
			this.loadMore.nativeElement.dispatchEvent(event);
		}
    
		showMoreMobile() {
			let event = new MouseEvent('click', {bubbles: true});
			event.stopPropagation();
			this.showMobile = this.showMobile + this.next;
      if(this.showMobile > this.items.length){
			this.hideMobile = false;    
      }
			this.loadMore.nativeElement.click();
			//this.loadMore.nativeElement.dispatchEvent(event);
		}
    
		animationStarted(event: AnimationEvent) {
			console.warn('Animation started: ', event);
		}
	
		animationDone(event: AnimationEvent) {
			console.warn('Animation done: ', event);
		}	
  ngOnInit() {}

}
