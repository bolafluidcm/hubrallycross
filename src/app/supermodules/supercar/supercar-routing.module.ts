import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupercarComponent } from './supercar.component';
import { FullLayoutComponent } from './components/full-layout.component';
import { IntermediatePageComponent } from './components/intermediate-page.component';


const routes: Routes = [
  { path: '',
    component: FullLayoutComponent,
    data: {title: 'Sunday Evening'},
    children: [
      {
        path: 'intermediate',
        component: IntermediatePageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupercarRoutingModule {}
