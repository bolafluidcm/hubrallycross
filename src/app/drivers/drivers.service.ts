/**
 * Created by hqbytes on 23/04/2017.
 */
// Observable Version
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Driver } from './driver';

@Injectable()
export class DriverService {
  private driversUrl = 'http://datatree.rxhub.fluidaa4.dyndns.info/api.php/drivers';  // URL to web API

  constructor (private http: Http) {}

  getDrivers(): Observable<Driver[]> {
    return this.http.get(this.driversUrl)
      .map(response => response.json() as Driver[])
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }


  getCurrentDrivers(): Observable<Driver[]> {
    return this.http.get(this.driversUrl)
      .map(response => response.json() as Driver[])
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  //
  // create(name: string): Observable<Driver> {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //
  //   return this.http.post(this.driversUrl, { name }, options)
  //     .map(this.extractData)
  //     .catch(this.handleError);
  // }

  // private extractData(res: Response) {
  //   let body = res.json() as Driver[];
  //   return body.data || { };
  // }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
