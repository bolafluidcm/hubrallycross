export class Driver {
  constructor(

    public id: number,
    public Entry_ID: number,
    public position: number,
    public forename: string,
    public surname: string,
    public country: string,
    public dob: string,
    public team: string,
    public car: string,
    public number: number,
    public img: string,
    public tag: string,
    public social_fb: string,
    public social_twit: string,
    public social_insta: string,
    public social_instagram_handle: string,
    public website: string,
    public description: string,
    public stats: string,
    public added: string,
    public admin_user: string,
    public timestamp: string,
    public series: string,
    public home_town: string,
    public interests: string,
    public car_img: string
  ) {}
}
