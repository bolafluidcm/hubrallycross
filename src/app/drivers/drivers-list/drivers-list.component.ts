import { Component, OnInit } from '@angular/core';
import { Driver } from '../driver';
import { DriverService } from '../drivers.service';

@Component({
  selector: 'app-drivers-list',
  templateUrl: '../../../assets/templates/drivers-list.html',
  providers: [ DriverService ],
  styles: []
})
export class DriversListComponent implements OnInit {
  errorMessage: string;
  drivers: Driver[];
  mode = 'Observable';



  constructor(private driverService: DriverService ) { }

  ngOnInit() { this.getCurrentDrivers();  }
  // ngOnInit() { this.getDrivers();  }

  // getDrivers() {
  //   this.driverService.getDrivers()
  //     .subscribe(
  //       drivers => this.drivers = drivers,
  //       error =>  this.errorMessage = <any>error);
  // }

  getCurrentDrivers() {
    this.driverService.getCurrentDrivers()
      .subscribe(
        drivers => this.drivers = drivers,
        error =>  this.errorMessage = <any>error);
  }

}
