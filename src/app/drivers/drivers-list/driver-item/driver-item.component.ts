import { Component, OnInit, Input } from '@angular/core';
import { Driver } from '../../driver';

@Component({
  selector: 'app-driver-item',
  templateUrl: '../../../assets/templates/driver-item.html'
})
export class DriverItemComponent implements OnInit {
	@Input() driver: Driver;
	driverId: number;

  constructor() { }

  ngOnInit() {
  }

}

