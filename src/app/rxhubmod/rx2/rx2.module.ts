import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Rx2Component } from './rx2.component';
import { Rx2RoutingModule } from './rx2-routing.module';
import { SharedModule } from '../../../app/shared/shared.module';
// import { SwiperModule } from 'angular2-useful-swiper';
import { NgPipesModule } from 'ngx-pipes';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    SharedModule,
    // SwiperModule,
    NgPipesModule,
    Rx2RoutingModule
  ],
  declarations: [ Rx2Component
  ],
  providers:    [ ]
})
export class Rx2Module { }





