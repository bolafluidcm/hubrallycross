import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Rx2Component } from './rx2.component';


const routes: Routes = [
  { path: '',
    component: Rx2Component,
    data: {title: 'Rx2'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Rx2RoutingModule {}
