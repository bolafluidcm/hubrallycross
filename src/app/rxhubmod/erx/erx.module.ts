import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ErxComponent } from './erx.component';

import { ErxRoutingModule } from './erx-routing.module';
import { SharedModule } from '../../../app/shared/shared.module';
// import { SwiperModule } from 'angular2-useful-swiper';
import { NgPipesModule } from 'ngx-pipes';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    SharedModule,
    // SwiperModule,
    NgPipesModule,
    ErxRoutingModule
  ],
  declarations: [
      ErxComponent
  ],
  providers:    [ ]
})
export class ErxModule { }





