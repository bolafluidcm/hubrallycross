import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErxComponent } from './erx.component';


const routes: Routes = [
  { path: '',
    component: ErxComponent,
    data: {title: 'Erx'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErxRoutingModule {}
