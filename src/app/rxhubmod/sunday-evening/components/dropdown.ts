export class Dropdown {
  constructor(
              public catid: number,
              public shortname: string,
              public title: string,
              public url: string  ) { }
}
