import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../../services/json-parse.service';
import { TruncatePipe } from '../../../shared/pipes/truncate-pipe';
import { animateFactory } from 'ng2-animate';
// import { environment } from '../../../environments/environment';
import { Routes, RouterModule, Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-championships-page',
  templateUrl: '../layouts/championships-page.html',
  styles: [],
  animations: [animateFactory(500, 0, 'ease-in')]
})
export class ChampionshipsPageComponent implements OnInit {
  // private supercar = environment.supercar;
  categories = ['dummy','supercar', 'super1600', 'touringcar','rx2','supercar_erx'];
  parentRoute: string;
  pageid: any;

  show: number = 10;
  showMobile: number = 5;
  next: number = 5;
  hide: boolean = true;
  totals;
  itemLength;
  items: Array<any>;
  @ViewChild('loadMore') loadMore:ElementRef;

  data: Observable<Array<any>>;

  constructor(private service: JsonParseService, private router: Router, private route: ActivatedRoute){
    // this.data = service.getChamps();
    // const catid = this.supercar;
    // service.getChamps(catid).subscribe(p=>this.items = p);

    this.router.events.subscribe((res) => {
      this.parentRoute = this.router.url.split('/')[2];
      this.pageid = this.categories.indexOf(this.parentRoute);
      // console.log(this.pageid);
      // console.log(this.pageid," how far");
      const catid = this.pageid;
      this.service.getChampsPage(catid)
        .subscribe(
          p=>this.items = p
        );
    });
  }


  showMore() {
    let event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.show = this.show + this.next;
    if(this.show > this.items.length){
      this.hide = false;
    }
    this.loadMore.nativeElement.click();
    // console.log('shown : ' + this.show );
    //this.loadMore.nativeElement.dispatchEvent(event);
  }
  showMoreMobile() {
    let event = new MouseEvent('click', {bubbles: true});
    event.stopPropagation();
    this.showMobile = this.showMobile + this.next;
    if(this.showMobile > this.items.length){
      this.hide = false;
    }
    this.loadMore.nativeElement.click();
  }

  animationStarted(event: AnimationEvent) {
    //console.warn('Animation started: ', event);
  }

  animationDone(event: AnimationEvent) {
    //console.warn('Animation done: ', event);
  }


  ngOnInit() {}
}
