import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { JsonParseService } from '../../../services/json-parse.service';
import _ from 'lodash';
import $ from 'jquery';
import { animateFactory } from 'ng2-animate';


@Component({
  selector: 'app-entrants-page',
  templateUrl: '../layouts/entrants-page.html',
  animations: [animateFactory(1000, 0, 'ease-in')]
})

export class EntrantsPageComponent implements OnInit {
  items: any[] = [];
  categories = ['dummy','supercar', 'super1600', 'touringcar','rx2','supercar_erx'];
  parentRoute: string;
  pageid: any;

  constructor(private service: JsonParseService, private router: Router, private route: ActivatedRoute) {


    this.router.events.subscribe((res) => {
      this.parentRoute = this.router.url.split('/')[2];
      this.pageid = this.categories.indexOf(this.parentRoute);
      const catid = this.pageid;
      service.getEntrantsPage(catid).subscribe(p=>this.items = p);

      // this.service.getEntrantsPage(catid).subscribe(
         //    data => {
         //      const grouped = _.groupBy(data, 'category');
         //      this.items = grouped;
         //      // console.log(catid);
         //    }
         //  );
    });

  }
    animationStarted(event: AnimationEvent) {
    }
    animationDone(event: AnimationEvent) {
    }
  ngOnInit() {
  }

}
