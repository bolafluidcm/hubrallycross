import { Component, OnInit, Renderer, ElementRef, ViewChild, ViewChildren } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes, group, AnimationEvent} from '@angular/animations';
import { Observable } from 'rxjs';
import { JsonParseService } from '../../../services/json-parse.service';
import {NewParseService } from '../../../services/newparse.service';
import { ExplodePipe } from '../../../shared/pipes/explode-pipe';
import { TruncatePipe } from '../../../shared/pipes/truncate-pipe';
import { animateFactory } from 'ng2-animate';
import { Routes, RouterModule, Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-intermediate-page',
  templateUrl: '../layouts/intermediate-page.html',
  styles: [],
  animations: [animateFactory(1000, 0, 'ease-in')]
})
export class IntermediatePageComponent implements OnInit {
	categories = ['dummy','supercar', 'super1600', 'touringcar','rx2','supercar_erx'];
	parentRoute: string;
  pageid: any;

	show: number = 10;
	showMobile: number = 5;
	next: number = 5;
	hide: boolean = true;
	hideMobile: boolean = true;
	data: Observable<Array<any>>;
	items: any[] = [];
	@ViewChild('loadMore') loadMore:ElementRef;

	constructor(private service: JsonParseService, private renderer: Renderer, private router: Router, private route: ActivatedRoute){
    this.router.events.subscribe((res) => {
      this.parentRoute = this.router.url.split('/')[2];
      this.pageid = this.categories.indexOf(this.parentRoute);
      // console.log(this.pageid);
      // console.log(this.pageid," how far");
      const koko = this.pageid;
      this.service.getIntermediatesPage(koko)
        .subscribe(
          p=>this.items = p
        );
    });

	}
	showMore() {
		let event = new MouseEvent('click', {bubbles: true});
		event.stopPropagation();
		this.show = this.show + this.next;
		if(this.show > this.items.length){
			this.hide = false;
		}
		//this.clickme.nativeElement.click();
		//this.renderer.invokeElementMethod(
		this.loadMore.nativeElement.dispatchEvent(event);
	}

	showMoreMobile() {
		let event = new MouseEvent('click', {bubbles: true});
		event.stopPropagation();
		this.showMobile = this.showMobile + this.next;
	if(this.showMobile > this.items.length){
		this.hideMobile = false;
	}
		this.loadMore.nativeElement.click();
		//this.loadMore.nativeElement.dispatchEvent(event);
	}

	animationStarted(event: AnimationEvent) {
		console.warn('Animation started: ', event);
	}
	animationDone(event: AnimationEvent) {
		console.warn('Animation done: ', event);
	}
  ngOnInit() {}

}
