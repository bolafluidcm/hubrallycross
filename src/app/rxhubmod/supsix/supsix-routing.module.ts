import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupsixComponent } from './supsix.component';


const routes: Routes = [
  { path: '',
    component: SupsixComponent,
    data: {title: 'Super1600'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupsixRoutingModule {}
