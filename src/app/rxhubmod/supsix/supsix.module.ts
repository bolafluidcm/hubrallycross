import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { JsonParseService } from '../../../app/services/json-parse.service';
import { NewParseService } from '../../../app/services/newparse.service';
import { SupsixComponent } from './supsix.component';
import { SupsixRoutingModule } from './supsix-routing.module';
import { SharedModule } from '../../../app/shared/shared.module';
// import { SwiperModule } from 'angular2-useful-swiper';
import { NgPipesModule } from 'ngx-pipes';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    SharedModule,
    // SwiperModule,
    NgPipesModule,
    SupsixRoutingModule
  ],
  declarations: [ SupsixComponent
  ],
  providers:    [ JsonParseService, NewParseService ]
})
export class SupsixModule { }





