import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TouringcarComponent } from './touringcar.component';


const routes: Routes = [
  { path: '',
    component: TouringcarComponent,
    data: {title: 'Touring Car'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TouringcarRoutingModule {}
