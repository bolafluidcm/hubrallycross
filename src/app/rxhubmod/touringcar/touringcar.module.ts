import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TouringcarComponent } from './touringcar.component';
import { TouringcarRoutingModule } from './touringcar-routing.module';
import { SharedModule } from '../../../app/shared/shared.module';
// import { SwiperModule } from 'angular2-useful-swiper';
import { NgPipesModule } from 'ngx-pipes';

@NgModule({
  imports:      [
    CommonModule,
    FormsModule,
    SharedModule,
    // SwiperModule,
    NgPipesModule,
    TouringcarRoutingModule
  ],
  declarations: [ TouringcarComponent
  ],
  providers:    [ ]
})
export class TouringcarModule { }





