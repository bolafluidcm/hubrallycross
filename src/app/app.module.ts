import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule, JsonpModule } from '@angular/http';
import { JsonParseService } from './services/json-parse.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';
import { MaterialModule } from '@angular/material';
// import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SwiperModule } from 'angular2-useful-swiper';

import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';
import { DriversComponent } from './drivers/drivers.component';

// Routing Module
import { AppRoutingModule } from './app.routing';
import { AuthGuardService } from './shared/auth-guard.service';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SharedModule } from 'app/shared/shared.module';
import { EntrantsComponent } from './race/entrants/entrants.component';
import { EventsComponent } from './events/events.component';
// import { EventrxComponent } from './eventrx/eventrx.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    JsonpModule,
    HttpModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    SwiperModule
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    DriversComponent,
    EntrantsComponent,
    EventsComponent
  ],
  providers: [
    AuthGuardService,
    JsonParseService, {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [ AppComponent ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
